#pragma once
#include <d3dx9.h>

class D3DDevice
{
public:

	LPDIRECT3D9 pD3d;
	LPDIRECT3DDEVICE9 pDevice;

	D3DDevice();
	~D3DDevice();

	bool CreateDevice(HWND hWnd, int Width, int Height);
};

